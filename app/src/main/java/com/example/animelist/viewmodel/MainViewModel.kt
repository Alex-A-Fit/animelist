package com.example.animelist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.animelist.model.models.models.Animes
import com.example.animelist.model.MainRepo
import com.example.animelist.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {
    val repo by lazy { MainRepo }

    val _animes: MutableLiveData<Resource>  = MutableLiveData()
    val anime: LiveData<Resource> get() = _animes

    fun getAnimes(data: Animes) = viewModelScope.launch(Dispatchers.Main) {
        val response = repo.getAnimes(data)
        _animes.value = response
    }


}