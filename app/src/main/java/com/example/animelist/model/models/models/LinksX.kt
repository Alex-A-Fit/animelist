package com.example.animelist.model.models.models

data class LinksX(
    val related: String,
    val self: String
)