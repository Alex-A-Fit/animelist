package com.example.animelist.model.models.models

data class LinksXX(
    val related: String,
    val self: String
)