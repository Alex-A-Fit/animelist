package com.example.animelist.model.models.models

data class Large(
    val height: Int,
    val width: Int
)