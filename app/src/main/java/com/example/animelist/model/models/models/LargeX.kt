package com.example.animelist.model.models.models

data class LargeX(
    val height: Int,
    val width: Int
)