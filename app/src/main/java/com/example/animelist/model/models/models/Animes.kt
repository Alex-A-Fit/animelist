package com.example.animelist.model.models.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Animes(
    val `data`: List<Data>
): Parcelable