package com.example.animelist.model

import com.example.animelist.model.models.models.Animes
import com.example.animelist.model.models.models.Data
import com.example.animelist.model.api.Api
import com.example.animelist.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object MainRepo {
    private val apiService = object : Api {
        override suspend fun getAnimes(animeData: Animes): List<Data> {
                return animeData.data        }
        }

    suspend fun getAnimes(data: Animes): Resource = withContext(Dispatchers.IO){
        delay(3000L)
        return@withContext Resource.Success(apiService.getAnimes(data))
    }
}