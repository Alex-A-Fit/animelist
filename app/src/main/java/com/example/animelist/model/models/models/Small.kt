package com.example.animelist.model.models.models

data class Small(
    val height: Int,
    val width: Int
)