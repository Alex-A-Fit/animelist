package com.example.animelist.model.models.models

data class SmallX(
    val height: Int,
    val width: Int
)