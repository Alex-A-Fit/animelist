package com.example.animelist.model.api

import com.example.animelist.model.models.models.Animes
import com.example.animelist.model.models.models.Data

interface Api {
    suspend fun getAnimes(animeData: Animes): List<Data>

}