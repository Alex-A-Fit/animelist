package com.example.animelist.model.models.models

data class Medium(
    val height: Int,
    val width: Int
)