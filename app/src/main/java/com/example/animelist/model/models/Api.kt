package com.example.animelist.model.models

import com.example.animelist.model.models.models.Animes

interface Api {
    suspend fun getAnimes(animeData: Animes)
}