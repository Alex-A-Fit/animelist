package com.example.animelist.model.models.models

import android.os.Parcelable
import com.example.animelist.model.models.models.Attributes
import kotlinx.parcelize.Parcelize

@Parcelize
data class Data(
    val attributes: Attributes,
    val id: String? = "",
    val type: String? = ""
): Parcelable