package com.example.animelist.model.models.models

data class TinyX(
    val height: Int,
    val width: Int
)