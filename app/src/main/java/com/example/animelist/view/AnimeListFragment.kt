package com.example.animelist.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.animelist.R
import com.example.animelist.model.models.models.Animes
import com.example.animelist.utils.getJsonDataFromAsset
import com.example.animelist.databinding.FragmentAnimeListBinding
import com.example.animelist.model.models.models.CoverImage
import com.example.animelist.model.models.models.Titles
import com.example.animelist.utils.Resource
import com.example.animelist.viewmodel.MainViewModel
import com.example.animelist.view.adapter.adapter
import com.google.gson.Gson

class AnimeListFragment : Fragment() {
    private var _binding: FragmentAnimeListBinding? = null
    private val binding: FragmentAnimeListBinding get() = _binding!!

    private val viewmodel by activityViewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentAnimeListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = getJsonDataFromAsset(requireContext(), "anime.json")
        val actualData = Gson().fromJson(data, Animes::class.java)
        viewmodel.getAnimes(actualData)
        onInitObserver()
    }

    private fun onInitObserver() = with(viewmodel){
        anime.observe(viewLifecycleOwner) {viewState ->
            when(viewState){
                Resource.Loading -> {
                    binding.pbCircularLoading.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.pbCircularLoading.visibility = View.INVISIBLE
                    binding.tvLoadingText.text = ""
                    binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
                    binding.recyclerView.adapter = adapter(::navigate).apply {
                        giveData(viewState.data)
                    }

                }
            }
        }
    }
    private fun navigate(coverImage: String, title: String, averageRating: String, contentRating: String, desc: String) {
        val action = AnimeListFragmentDirections.actionAnimeListFragmentToAnimeDetailsFragment( title, averageRating, contentRating, coverImage, desc )
        findNavController().navigate(action)
    }
}