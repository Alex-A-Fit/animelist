package com.example.animelist.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.animelist.model.models.models.Data
import com.bumptech.glide.Glide
import com.example.animelist.databinding.ListItemBinding

 class adapter(
    private val navigate: (coverImage: String, title: String, averageRating: String, contentRating: String, desc: String) -> Unit
) : RecyclerView.Adapter<adapter.viewHolder>() {
    private lateinit var data: List<Data>
    private var _binding : ListItemBinding? = null
    private val binding: ListItemBinding get() = _binding!!

    class viewHolder(private val binding: ListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun apply(data: Data) = with(binding){
            if(data.attributes.slug.length > 18){
                listItemName.textSize = 18.0F
            } else if(data.attributes.slug.length > 15){
                listItemName.textSize = 20.0F
            }
            else{
                listItemName.textSize = 24.0F
            }
            listItemName.text = data.attributes.slug.uppercase()
            imageView.loadImage(data.attributes.posterImage.small)
        }
//        fun getDetails(data: Data): List<String> {
//            return listOf(
//                data.attributes.slug.uppercase(),
//                data.attributes.coverImage.large,
//                data.attributes.averageRating,
//                data.attributes.ageRating,
//                data.attributes.description
//            )
//        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false )
        return viewHolder(binding)
//            .apply() {
//            val item = data[viewType]
//            val data = getDetails(item)
//            binding.root.setOnClickListener {
//            navigate(data[1], data[0], data[2], data[3], data[4])
//            }
//        }
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val item = data[position]
        holder.itemView.setOnClickListener {
            navigate(item.attributes.coverImage.large, item.attributes.slug.uppercase(),  item.attributes.averageRating, item.attributes.ageRating, item.attributes.description)
        }
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }
    fun giveData(data: List<Data>){
        this.data = data
    }
}
private fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}
