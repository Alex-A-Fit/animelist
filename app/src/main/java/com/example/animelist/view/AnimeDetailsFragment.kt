package com.example.animelist.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.animelist.R
import com.example.animelist.databinding.FragmentAnimeDetailsBinding
import kotlin.math.roundToInt

class AnimeDetailsFragment : Fragment() {
    private var _binding: FragmentAnimeDetailsBinding? = null
    private val binding: FragmentAnimeDetailsBinding get() = _binding!!

    val args by navArgs<AnimeDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentAnimeDetailsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit()
    }

    private fun onInit() = with(binding){
        val avgRating = args.averageRating.toDouble().roundToInt()
        when {
            avgRating >= 90 -> {
                ivStar1.setImageResource(R.drawable.filled_star)
                ivStar2.setImageResource(R.drawable.filled_star)
                ivStar3.setImageResource(R.drawable.filled_star)
                ivStar4.setImageResource(R.drawable.filled_star)
                ivStar5.setImageResource(R.drawable.filled_star)
            }
            avgRating > 80 -> {
                ivStar1.setImageResource(R.drawable.filled_star)
                ivStar2.setImageResource(R.drawable.filled_star)
                ivStar3.setImageResource(R.drawable.filled_star)
                ivStar4.setImageResource(R.drawable.filled_star)
            }
            avgRating > 60 -> {
                ivStar1.setImageResource(R.drawable.filled_star)
                ivStar2.setImageResource(R.drawable.filled_star)
                ivStar3.setImageResource(R.drawable.filled_star)
            }
            avgRating > 40 -> {
                ivStar1.setImageResource(R.drawable.filled_star)
                ivStar2.setImageResource(R.drawable.filled_star)
            }
            avgRating > 20 -> {
                ivStar1.setImageResource(R.drawable.filled_star)
            }
        }
       imageView.loadImage(args.coverImage)
        tvTitle.text = args.title
        tvContentRatingDesc.text = args.contentRating
        tvAnimeDesc.text = args.animeDesc
    }
    private fun ImageView.loadImage(url: String) {
        Glide.with(context).load(url).into(this)
    }

}