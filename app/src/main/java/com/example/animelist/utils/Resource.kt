package com.example.animelist.utils

import com.example.animelist.model.models.models.Data

sealed class Resource(data: List<Data>?){
    data class Success(val data: List<Data>): Resource(data)
    object Loading: Resource(null)
}

